USE Modernways;
-- verwijdert de tabel 'Postcodes' indien hij al bestaat
DROP TABLE IF EXISTS `Postcodes`;
CREATE TABLE Postcodes(
	Postcode CHAR(4),
    Plaats NVARCHAR(120),
    Localite NVARCHAR(120),
    Provincie NVARCHAR(120),
    Province NVARCHAR(120)
);