use modernways;
insert into boekennaarauteurs(Auteurs_Id, Boeken_Id)
select  auteurs.Id, boeken.Id
from auteurs inner join boeken
where (auteurs.Familienaam = 'Murakami' and auteurs.Voornaam = 'Haruki' and boeken.Titel = ('Norwegian Wood'))
or (auteurs.Familienaam = 'Murakami' and auteurs.Voornaam = 'Haruki' and boeken.Titel = ('Kafka on the Shore'))
or (auteurs.Familienaam = "Gaiman" and auteurs.Voornaam = "Neil" and boeken.Titel = "Americand Gods")
or (auteurs.Familienaam = "Gaiman" and auteurs.Voornaam = "Neil" and boeken.Titel = "The Ocean at the End of the Lane")
or (auteurs.Familienaam = "King" and auteurs.Voornaam = "Stephen" and boeken.Titel = "Pet Sematary")
or (auteurs.Familienaam = ("Pratchett") and auteurs.Voornaam = ("Terry") and boeken.Titel = "Good Omens")
or (auteurs.Familienaam = ("Gaiman") and auteurs.Voornaam = ("Neil") and boeken.Titel = "Good Omens")
or (auteurs.Familienaam = "King" and auteurs.Voornaam = "Stephen" and boeken.Titel = "The Talisman")
or (auteurs.Familienaam = "Straub" and auteurs.Voornaam = "Peter" and boeken.Titel = "The Talisman");