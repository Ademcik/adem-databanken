use modernways;
create table TakenToLeden(
Leden_Id int not null, 
taken_Id int not null);

alter table TakenToLeden
ADD FOREIGN KEY fk_TakenToLeden_Taken(Taken_Id)
REFERENCES Taken(ID);

ALTER TABLE TakenToLeden
ADD FOREIGN KEY fk_TakenToLeden_Leden(Leden_Id)
references Leden(Id);