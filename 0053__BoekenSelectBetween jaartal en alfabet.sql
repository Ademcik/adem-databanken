/*: Selecteer de voornaam, familienaam, titel uit de tabel Boeken die gepubliceerd
 werden tussen 1900 en 2011 en waarvan de familienaam begint met de letter B. Orden de titels alfabetisch.*/
USE ModernWays;
SELECT Voornaam, Familienaam, Titel FROM boeken 
WHERE Boeken.Familienaam like 'b%' AND
Boeken.Verschijningsdatum BETWEEN '1900' AND '2011'
ORDER BY Boeken.Titel