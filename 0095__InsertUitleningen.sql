use modernways;

insert into uitleningen(Naam_Id , Naam ,Boek_Id, Boek)
select leden.Id,leden.Voornaam , boeken.Id, boeken.Titel from leden inner join boeken
where (leden.Voornaam = "Max" and boeken.Titel = "Norwegian Wood")
or (leden.Voornaam = "Bavo" and boeken.Titel = "Norwegian Wood")
or (leden.Voornaam = "Bavo" and boeken.Titel = "Pet Sematary")
or (leden.Voornaam = "Yannick" and boeken.Titel = "Pet Sematary");

update  uitleningen
set Start_Datum = '2019-02-01', Eind_Datum = '2019-02-15'
where (uitleningen.naam = "Max" and uitleningen.Boek = "Norwegian Wood");

update  uitleningen
set Start_Datum = '2019-02-16', Eind_Datum = '2019-03-02'
where (uitleningen.naam = "Bavo" and uitleningen.Boek = "Norwegian Wood");

update  uitleningen
set Start_Datum = '2019-02-16', Eind_Datum = '2019-03-02'
where (uitleningen.naam = "Bavo" and uitleningen.Boek = "Pet Sematary");

update  uitleningen
set Start_Datum = '2019-05-01'
where (uitleningen.naam = "Yannick" and uitleningen.Boek = "Pet Sematary");
