use ModernWays;
insert into Boeken (
   Voornaam,
   Familienaam,
   Titel,
   Stad,
   Uitgeverij,
   Verschijningsdatum,
   Herdruk,
   Commentaar,
   Categorie
  ) values
  ('Emile', 'Benveniste', 'Le vocabulaire des institutions Indo-Europeennes. 2. Pouvoir droit religion', 'Paris?', 'Les ditions de minuit', '1969', '', 'Een goed geschiedenis boek','Linguistiek'),
  ('Pierre', 'Bonte en Michel Izard', 'Dictionnaire de l''etnologie et de l''anthropologie', '?', 'PUF', '1991', '?', 'Een goed boek','Anthropologie');