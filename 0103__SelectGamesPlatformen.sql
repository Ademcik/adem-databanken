use modernways;

select platformen.Naam
from platformen left join releases
on platformen.id = releases.platformen_id
where releases.platformen_id is null
union
select games.Titel
from games left join releases
on games.id = releases.games_id
where releases.games_id is null;

