use modernways;
drop table if exists Uitleningen;
-- Ik denk dat de opdracht bestaat uit De huurder zijn naam te koppelen aan het boek
-- We gaan gewoon de Id's hiervoor gebruiken
-- Is de naam zelf ook nodig?
-- Datums zijn gewoon zelf in te vullen neem ik aan?
create table Uitleningen (
naam_Id int,
naam varchar (50),
boek_Id int,
boek varchar(50),
Start_Datum date, 
Eind_Datum date,
-- We linken eerst en vooral de foreign keys zodat We de juiste Id's achteraf kunnen ophalen en ze zo aan elkaar kunnen koppelen in het volgende script. 
constraint FK_Naam_leden foreign key (naam_Id) References leden (Id),
constraint FK_Boek_Boeken foreign key (Boek_id) references boeken(Id)
);