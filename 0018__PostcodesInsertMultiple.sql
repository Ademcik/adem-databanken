use Modernways;

insert into postcodes (
Code,
	Plaats,
    Localite,
    Provincie,
    Province 
)
values 
(2800, 'Mechelen','Maline', 'Antwerpen', 'Anvers'),
(3000, 'Leuven', 'Louvain', 'Vlaams-Brabant', 'Brabant-flamand');