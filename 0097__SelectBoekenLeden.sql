use modernways;

SELECT leden.Voornaam, boeken.Titel
FROM uitleningen
     INNER JOIN leden ON uitleningen.Naam_Id = leden.Id
     INNER JOIN boeken ON uitleningen.Boek_Id = boeken.Id