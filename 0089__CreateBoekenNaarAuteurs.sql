use modernways;

drop table if exists BoekenNaarAuteurs;

create table BoekenNaarAuteurs

select auteurs.Voornaam, boeken.Titel
from boeken
INNER JOIN auteurs on boeken.id = auteurs.i