use modernways;

Alter table Leden
Drop foreign key fk_Taken_id,
drop column Taken_id;

alter table Taken
add column Leden_Id int,
add foreign key fk_Taken_Leden(Leden_Id)
references Leden(Id);