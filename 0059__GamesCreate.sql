use ModernWays;
drop table if exists Game;
create table Game(
    Titel nvarchar(255) not null,
    Ontwikkelaar nvarchar(255) not null,
    Id int auto_increment PRIMARY KEY
);