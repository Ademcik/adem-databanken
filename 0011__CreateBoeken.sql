USE ModernWays;

DROP TABLE IF EXISTS Boeken;

CREATE TABLE Boeken(
    Voornaam NVARCHAR(50),
    Familienaam NVARCHAR(80),
    Titel NVARCHAR(255),
    Stad NVARCHAR(50),

    Verschijningsjaar CHAR(4),
    Uitgeverij NVARCHAR(80),
    Herdruk CHAR(4),
    Commentaar TEXT
);