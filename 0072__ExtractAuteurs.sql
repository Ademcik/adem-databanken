use Modernways;

Create table Auteurs (Voornaam varchar(255),Familienaam varchar(255),ID int auto_increment primary key);

-- stap 2 
insert into Auteurs(Voornaam, Familienaam)
select distinct Voornaam, Familienaam
from boeken,

-- stap 3 
alter table boeken
add column Auteurs_Id int,
add foreign key fk_boeken_Auteurs(Auteurs_Id)
references Auteurs(id);

-- stap 4
update boeken, Auteurs
set Auteurs_Id = Auteurs.Id
where ((Auteurs.Voornaam IS NULL AND boeken.Voornaam IS NULL) OR
	   (Auteurs.Voornaam = boeken.Voornaam)) AND
       ((Auteurs.Familienaam IS NULL AND boeken.Familienaam IS NULL) OR
	    (Auteurs.Familienaam = boeken.Familienaam);
    
-- stap 5
alter table boeken
modify column Auteurs_Id int not null;

-- stap 6
alter table boeken


